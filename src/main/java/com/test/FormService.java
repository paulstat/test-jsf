package com.test;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

@Named
public enum FormService
{
    
    INSTANCE(new ArrayList<Form>());
    
    private List<Form> forms;
    
    private FormService(List<Form> forms)
    {
        this.forms = forms;
        this.forms.add(new Form("Form 1"));
    }
    
    public void createForm(Form form)
    {
        forms.add(form);
    }
    
    public List<Form> findAll()
    {
        return forms;
    }

}
