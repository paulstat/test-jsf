package com.test;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class FormController
{
    
    private FormService formService;
    
    private List<Form> forms;
    
    public Form form = new Form();
    
    @PostConstruct
    public void init()
    {
        formService = FormService.INSTANCE;
        forms = formService.findAll();
    }
    
    public void createForm()
    {
        formService.createForm(form);
        form = new Form();
    }
    
    public List<Form> getForms()
    {
        return forms;
    }
    
    public void setForms(List<Form> forms)
    {
        this.forms = forms;
    }
    
    public Form getForm()
    {
        return form;
    }

    public void setForm(Form form)
    {
        this.form = form;
    }
    
}
