package com.test;

public class Form
{
    
    private String name;
    
    public Form()
    {
        
    }
    
    public Form(String name)
    {
        this.name = name;
    }

     public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
}
